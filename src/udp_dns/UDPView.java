package udp_dns;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class UDPView {
	//The model of the event allocator
    private DNSRequest DNSRequest;
    // root node for the view
    private GridPane gridPane;
    // The Event Name text field.
    private TextField domainSearchBox;
    //Font type for all ListView, TableView and Instructions label's
    private Font font = Font.font("SanSerif", FontWeight.MEDIUM, 20);
    //Font type for the "Create a Event" Label
    private Font titleFont = Font.font("SanSerif", FontWeight.BOLD, 25);
    //Font type for textField's and Instructions
    private Font fontTextField = Font.font("SanSerif", FontWeight.MEDIUM, 14);
    //Add event button
    private Button searchDomain;
    //Private ListView for the Venue Capacity
    private ListView<Integer> venueCapacityList;
    
    private final Label instructionsLabel = new Label("Requested (Host) Name: ");
    
    
    /**
     * Creates a new Event Allocator view with the given model.
     * 
     * @param model
     *            the model of the event allocator
     */
    public UDPView(DNSRequest DNSRequest) {
    	this.DNSRequest = DNSRequest;
        gridPane = new GridPane();
        addMainScreenDisplay();
        hostNameDisplay();
        ip4Display();
        ip6Display();
        CNAMEDisplay();
        MXDisplay();
    }
    
    private void addMainScreenDisplay() {
		
		//Initialising the text fields
		domainSearchBox = new TextField();
		
		//Allow both text fields to be editable
		domainSearchBox.setEditable(true);
		
		//Creating the font for domainSearchBox
		domainSearchBox.setFont(fontTextField);
		
		//Setting the Alignment of each TextField
		domainSearchBox.setAlignment(Pos.CENTER_LEFT);

		//Setting the text fields prompted text
		domainSearchBox.setPromptText("Event Name");
		
		//Adding a header text, setting the font style and adding to gridPane
		Text domainSearchText = new Text (0, 0, "Search a domain");
		domainSearchText.setFont(titleFont);
		gridPane.add(domainSearchText, 0, 2);
		

		searchDomain = new Button("Allocate Event");
		searchDomain.setTextFill(Color.CORNFLOWERBLUE);
		
		/* Creating a new HBox and adding all TextField's and Button's */
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(domainSearchBox, searchDomain);
        //Adding spacing between each Child within ButtonBox
        buttonBox.setSpacing(3);
        
        gridPane.add(buttonBox, 0, 3); // Column 0, Row 
        
      //Setting the background color to a light pink
        gridPane.setStyle("-fx-background-color: #efdef2");
        
    }
    
    /**
     * Returns the scene for the event allocator application.
     * 
     * @return returns the scene for the application
     */
    public Scene getScene() {
    	return new Scene(gridPane, 0, 0);
    }
    
    
    public void ip4Display(){
    	//Creating an empty ObservableList
    	ObservableList<String> items = FXCollections.observableArrayList();
    	//Adding to the ObservableList if the eventNameList is not empty
    	if(!(DNSRequest.getIP4List().isEmpty())){
    		items.addAll(DNSRequest.getIP4List());
    	}
    	//Creating new local ListView eventNameList
    	ListView<String> ip4List = new ListView<String>();
        //Assigning the current ObsrvableList to the ListView
    	ip4List.setItems(items);
    	//Setting the Height and width of the ListView
        ip4List.setPrefHeight(400);
        ip4List.setPrefWidth(100);
        
        //Creating a Label for the list and setting its font
        final Label ip4Label = new Label("IPv4 Address");
        ip4Label.setFont(font);
        
        //Creating a new VBox and adding its corresponding Label and ListView
        VBox ip4Vbox = new VBox();
        ip4Vbox.getChildren().addAll(ip4Label, ip4List);
        ip4Vbox.setSpacing(5);
        ip4Vbox.setPrefSize(200, 500);
        //Adding the VBox to GridPane at column 1 row 0
        gridPane.add(ip4Vbox, 1, 0);
    }
    
    public void ip6Display(){
    	//Creating an empty ObservableList
    	ObservableList<String> items = FXCollections.observableArrayList();
    	//Adding to the ObservableList if the eventNameList is not empty
    	if(!(DNSRequest.getIP6List().isEmpty())){
    		items.addAll(DNSRequest.getIP6List());
    	}
    	//Creating new local ListView eventNameList
    	ListView<String> ip6List = new ListView<String>();
        //Assigning the current ObsrvableList to the ListView
    	ip6List.setItems(items);
    	//Setting the Height and width of the ListView
        ip6List.setPrefHeight(400);
        ip6List.setPrefWidth(100);
        
        //Creating a Label for the list and setting its font
        final Label ip6Label = new Label("IPv6 Address");
        ip6Label.setFont(font);
        
        //Creating a new VBox and adding its corresponding Label and ListView
        VBox ip6Vbox = new VBox();
        ip6Vbox.getChildren().addAll(ip6Label, ip6List);
        ip6Vbox.setSpacing(5);
        ip6Vbox.setPrefSize(200, 500);
        //Adding the VBox to GridPane at column 2 row 0
        gridPane.add(ip6Vbox, 2, 0);
    }
    
    public void CNAMEDisplay(){
    	//Creating an empty ObservableList
    	ObservableList<String> items = FXCollections.observableArrayList();
    	//Adding to the ObservableList if the eventNameList is not empty
    	if(!(DNSRequest.getCNAMEList().isEmpty())){
    		items.addAll(DNSRequest.getCNAMEList());
    	}
    	//Creating new local ListView eventNameList
    	ListView<String> cnameList = new ListView<String>();
        //Assigning the current ObsrvableList to the ListView
    	cnameList.setItems(items);
    	//Setting the Height and width of the ListView
        cnameList.setPrefHeight(400);
        cnameList.setPrefWidth(100);
        
        //Creating a Label for the list and setting its font
        final Label cnameLabel = new Label("CNAME (Canonical Name)");
        cnameLabel.setFont(font);
        
        //Creating a new VBox and adding its corresponding Label and ListView
        VBox cnameVbox = new VBox();
        cnameVbox.getChildren().addAll(cnameLabel, cnameList);
        cnameVbox.setSpacing(5);
        cnameVbox.setPrefSize(200, 500);
        //Adding the VBox to GridPane at column 3 row 0
        gridPane.add(cnameVbox, 3, 0);
    }
    
    
    public void MXDisplay(){
    	//Creating an empty ObservableList
    	ObservableList<String> items = FXCollections.observableArrayList();
    	//Adding to the ObservableList if the eventNameList is not empty
    	if(!(DNSRequest.getMXList().isEmpty())){
    		items.addAll(DNSRequest.getMXList());
    	}
    	//Creating new local ListView eventNameList
    	ListView<String> MXList = new ListView<String>();
        //Assigning the current ObsrvableList to the ListView
    	MXList.setItems(items);
    	//Setting the Height and width of the ListView
        MXList.setPrefHeight(400);
        MXList.setPrefWidth(100);
        
        //Creating a Label for the list and setting its font
        final Label MXLabel = new Label("Mail Server Address");
        MXLabel.setFont(font);
        
        //Creating a new VBox and adding its corresponding Label and ListView
        VBox MXVbox = new VBox();
        MXVbox.getChildren().addAll(MXLabel, MXList);
        MXVbox.setSpacing(5);
        MXVbox.setPrefSize(250, 500);
        //Adding the VBox to GridPane at column 4 row 0
        gridPane.add(MXVbox, 4, 0);
    }
    
    
    /**
     * Add handler to the searchDomain operation.
     * 
     * @param handler
     *            the handler to be added
     */
    public void searchDomainHandler(EventHandler<ActionEvent> handler) {
        searchDomain.setOnAction(handler);;
    }
    
    /**
     * Get the root node of the scene graph.
     * 
     * @return returns the root node in the scene graph for the view
     */
    public GridPane getRootNode() {
        return gridPane;
    }
    
    /**
     * Get the text in the searchDomain TextField .
     * 
     * @return return the text inside the eventName TextField
     */
    public String getDomainNameText() {
        return domainSearchBox.getText();
    }
    
    /**
     * Clear the searchDomain TextField.
     */
    public void clearDomainNameBox() {
        searchDomain.setText("");
    }
    
    public Label hostNameDisplay(){
        //Creating a label for above the instructions and setting the font
        instructionsLabel.setFont(font);
        //Creating new Instruction VBox
        VBox instructionsVbox = new VBox();
        instructionsVbox.getChildren().addAll(instructionsLabel);
        //Setting VBox to column 0 row 1
        gridPane.add(instructionsVbox, 0, 1);
        return instructionsLabel;
    }
}
