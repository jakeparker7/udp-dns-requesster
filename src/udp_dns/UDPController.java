package udp_dns;


import javafx.scene.control.Label;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import udp_dns.*;

public class UDPController {
	// the model of the dns requester
    private DNSRequest DNSRequest;
    // the view of the dns requester
    private UDPView view;
    
    public UDPController(DNSRequest DNSRequest, UDPView view) {
    	this.DNSRequest = DNSRequest;
    	this.view = view;
    	//Create new handlers for actions
    	view.searchDomainHandler(new NewEventHandler());
    }
    
    
    private class NewEventHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
        	try{
        		//Handling any input error's in the TextField
        		domainErrorHandler();
        		//Creating a new domain search
        		DNSRequest domainRequested = new DNSRequest(view.getDomainNameText());
        		view.ip4Display();
        		view.ip6Display();
        		view.CNAMEDisplay();
        		view.MXDisplay();
        		Label hostNameLabel = view.hostNameDisplay();
        		hostNameLabel.setText("Requested (Host) Name: " + DNSRequest.getDomain());
        	} catch (Exception e) {
        		//Catch all potential exceptions and throw a warning message
        		String errorMessage = e.getMessage();
        		DNSRequest.warningAlert(errorMessage);
        		
        	}
        }
    }
    
    
    private void domainErrorHandler() {
    	if(!(view.getDomainNameText().contains("."))) {
    		DNSRequest.warningAlert("Invalid Domain Request");
    	}

//TODO: Try catch unkown host exception
    }
}
