package udp_dns;
import java.io.ByteArrayInputStream;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A quick and dirty example of query DNS A record and log response.
 * This code has no error handling.
 *
 */

public class DNSRequest {
	
    private static final String DNS_SERVER_ADDRESS = "8.8.8.8";
    private static final int DNS_SERVER_PORT = 53;
    public static List<String> address4List;
    public static List<String> address6List;
    public static List<String> cnameList;
    public static List<String> MXList;
    public static String domain;
    public static InetAddress ipAddress;
    public static DatagramSocket socket;
    
    public DNSRequest(String domain) throws IOException {
    	DNSRequest.domain = domain;
        socket = null;
        address4List = new ArrayList<String>();
        address6List = new ArrayList<String>();
        cnameList = new ArrayList<String>();
        MXList = new ArrayList<String>();
        //Allowed to use according to Piazza @152 Only time other libraries are used,
        //the rest is done by interpreting replies. Was needed for Java DNS code. All other
        //DNS Queries are done raw and no libraries are used.
        ipAddress = InetAddress.getByName(DNS_SERVER_ADDRESS);
        if (ip(domain) == true){
        	System.out.println(ipAddress + "In if");
        	reverseDNSFetch();
        	receiver();
        	ip4Setup();
            receiver();
            ip6Setup();
            receiver();
            MXSetup();
            receiver();
        } else {
            System.out.println(ipAddress + "In else");
            ip4Setup();
            receiver();
            ip6Setup();
            receiver();
            MXSetup();
            receiver();
        }
        
        System.out.println("IP 4 Address List: " + address4List);
        System.out.println("IP 6 Address List: " + address6List);
        System.out.println("MX Address List" + MXList);
        System.out.println("CNAME Address List" + cnameList);
    }
    
    
    public static void receiver() throws IOException {
    	// Await response from DNS server
        byte[] buf = new byte[2048];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        socket.receive(packet);

        System.out.println("\n\nReceived: " + packet.getLength() + " bytes");

        for (int i = 0; i < packet.getLength(); i++) {
            System.out.print(" 0x" + String.format("%x", buf[i]) + " " );
        }
        System.out.println("\n");

        DataInputStream din = new DataInputStream(new ByteArrayInputStream(buf));
        System.out.println("Transaction ID: 0x" + String.format("%x", din.readShort()));
        System.out.println("Flags: 0x" + String.format("%x", din.readShort()));
        System.out.println("Questions: 0x" + String.format("%x", din.readShort()));
        System.out.println("Answers RRs: 0x" + String.format("%x", din.readShort()));
        System.out.println("Authority RRs: 0x" + String.format("%x", din.readShort()));
        System.out.println("Additional RRs: 0x" + String.format("%x", din.readShort()));
        
        //Reading "Queries" &  "Answers" Hex
        int recLen = 0;
        while ((recLen = din.readByte()) > 0) {
            byte[] record = new byte[recLen];

            for (int i = 0; i < recLen; i++) {
                record[i] = din.readByte();
            }

            System.out.println("Record: " + new String(record, "UTF-8"));
        }
        System.out.println("Record Type: 0x" + String.format("%x", din.readShort()));
        System.out.println("Class: 0x" + String.format("%x", din.readShort()));
        String type = "";
        while(!(type.equals("0"))) {
	        System.out.println("Field: 0x" + String.format("%x", din.readShort()));
	        type = String.format("%x", din.readShort());
	        System.out.println("Type: 0x" + type);
	       
	        //Handle a CNAME situation
	        if(type.equals("5")) {
	        	System.out.println("***Handling CNAME***");
	        	System.out.println("Class: 0x" + String.format("%x", din.readShort()));
	        	System.out.println("TTL: 0x" + String.format("%x", din.readInt()));
	        	short cnameLength = din.readShort();
	        	//System.out.println("Len: 0x" + String.format("%x", cnameLength));
	        	//System.out.println(din.readByte());
	        	//short cnameLength;
		        while ((cnameLength = din.readByte()) > 0) {
		            byte[] record = new byte[cnameLength];
		
		            for (int i = 0; i < cnameLength; i++) {
		                record[i] = din.readByte();
		            }
		            String cnameString =  new String(record, "UTF-8");
		            String lastCharacter = cnameString.substring(cnameString.length() - 1);
		            String hostName;
		            if(lastCharacter.equals(".")) {
		            	hostName = cnameString.substring(0, cnameString.length() - 1);
		            } else {
		            	String domainSplit[] = domain.split("\\.", 2);
		            	System.out.println(domain);
		            	hostName = cnameString + "." + domainSplit[1];
		            }
		            if(!(cnameList.contains(hostName))){
		            	cnameList.add(hostName);
		            }
		            System.out.println("Host Name: " + hostName);
		        }
		        din.readByte();
	        } else if(type.equals("1")) {
	             System.out.println("Class: 0x" + String.format("%x", din.readShort()));
	             System.out.println("TTL: 0x" + String.format("%x", din.readInt()));
	
	             short addrLen = din.readShort();
	             String lengthString = String.format("%x", addrLen);
	             System.out.println("Len: 0x" + lengthString);
	
	             System.out.print("Address: ");
	             String address = "";
	             for (int i = 0; i < addrLen; i++ ) {
	            	 address = address + "" + String.format("%d", (din.readByte() & 0xFF)) + ".";
	                 
	             }
	             address = address.substring(0, address.length() - 1);
	             System.out.print(address);
	             address4List.add(address);
	        } else if(type.equals("1c")) {
	        	 System.out.println("Class: 0x" + String.format("%x", din.readShort()));
	             System.out.println("TTL: 0x" + String.format("%x", din.readInt()));
	
	             short addrLen = din.readShort();
	             String lengthString = String.format("%x", addrLen);
	             System.out.println("Len: 0x" + lengthString);
	             System.out.print("Address: ");
	             String address = "";
	             for(int i = 0; i < 8; i++) {
	            	 address = address + String.format("%x", din.readShort()) + ".";
	             }
	             address = address.substring(0, address.length() - 1);
	             address6List.add(address);
	        } else if(type.equals("f")) {
	        	 System.out.println("**Handling MX**");
	        	 System.out.println("Class: 0x" + String.format("%x", din.readShort()));
	             System.out.println("TTL: 0x" + String.format("%x", din.readInt()));
	             
	             short addrLen = din.readShort();
	             String lengthString = String.format("%x", addrLen);
	             System.out.println("Len: 0x" + lengthString);
	             String preferenceHex = String.format("%x", din.readShort());
	             int preferenceDecimal = Integer.parseInt(preferenceHex ,16);
	             System.out.println("Preference: " + preferenceDecimal);
	             System.out.print("Address: ");
	             short cnameLength;
	             while ((cnameLength = din.readByte()) > 0) {
	            	 byte[] record = new byte[cnameLength];
			
			         for (int i = 0; i < cnameLength; i++) {
			            record[i] = din.readByte();
			         }
			         String MXString =  new String(record, "UTF-8");
			         String lastCharacter = MXString.substring(MXString.length() - 1);
			         String hostName;
			         if(lastCharacter.equals(".")) {
			           	hostName = MXString.substring(0, MXString.length() - 1);
			         } else {
			          	String domainSplit[] = domain.split("\\.", 2);
			           	System.out.println(domain);
			           	hostName = MXString + "." + domainSplit[1];
			         }
			         MXList.add(hostName);
			         System.out.println("Host Name: " + hostName);
			     }
	             din.readByte(); 
	        } else if (type.equals("c")) {
	        	 System.out.println("**Handling Reverse DNS**");
	        	 System.out.println("Class: 0x" + String.format("%x", din.readShort()));
	             System.out.println("TTL: 0x" + String.format("%x", din.readInt()));
	             short addrLen = din.readShort();
	             String lengthString = String.format("%x", addrLen);
	             System.out.println("Len: 0x" + lengthString);
	             System.out.print("Address: ");
	             short domainLength;
	             domain = "";
	             while ((domainLength = din.readByte()) > 0) {
	            	 byte[] record = new byte[domainLength];
			
			         for (int i = 0; i < domainLength; i++) {
			            record[i] = din.readByte();
			         }
			         String domainParts =  new String(record, "UTF-8");
			         domain = domain + domainParts + ".";
	             }
	             domain = domain.substring(0, domain.length() - 1);
	        }
       socket.close();
       //TODO: close all other input output streams.
       }
    }
    
    public static void ip4Setup() throws IOException {
    	ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        DataOutputStream dataOS = new DataOutputStream(byteArrayOS);

        // *** Build a DNS Request Frame ****

        // Identifier
        dataOS.writeShort(0x1234);

        // Write Query Flags
        dataOS.writeShort(0x0100);

        // Question Count
        dataOS.writeShort(0x0001);

        // Answer Record Count
        dataOS.writeShort(0x0000);

        // Authority Record Count
        dataOS.writeShort(0x0000);

        // Additional Record Count
        dataOS.writeShort(0x0000);

        String[] domainParts = domain.split("\\.");
        System.out.println(domain + " has " + domainParts.length + " parts");

        for (int i = 0; i<domainParts.length; i++) {
            System.out.println("Writing: " + domainParts[i]);
            byte[] domainBytes = domainParts[i].getBytes("UTF-8");
            dataOS.writeByte(domainBytes.length);
            dataOS.write(domainBytes);
        }

        // No more parts
        dataOS.writeByte(0x00);

        // Type 0x01 = A (Host Request)
        dataOS.writeShort(0x0001);

        // Class 0x01 = IN
        dataOS.writeShort(0x0001);

        byte[] dnsFrame = byteArrayOS.toByteArray();

        System.out.println("Sending: " + dnsFrame.length + " bytes");
        for (int i =0; i< dnsFrame.length; i++) {
            System.out.print("0x" + String.format("%x", dnsFrame[i]) + " " );
        }

        // *** Send DNS Request Frame ***
        socket = new DatagramSocket();
        DatagramPacket dnsReqPacket = new DatagramPacket(dnsFrame, dnsFrame.length, ipAddress, DNS_SERVER_PORT);
        socket.send(dnsReqPacket);
    }
    
    
    public static void ip6Setup() throws IOException {
    	ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        DataOutputStream dataOS = new DataOutputStream(byteArrayOS);

        // *** Build a DNS Request Frame ****

        // Identifier
        dataOS.writeShort(0x1234);

        // Write Query Flags
        dataOS.writeShort(0x0100);

        // Question Count
        dataOS.writeShort(0x0001);

        // Answer Record Count
        dataOS.writeShort(0x0000);

        // Authority Record Count
        dataOS.writeShort(0x0000);

        // Additional Record Count
        dataOS.writeShort(0x0000);

        String[] domainParts = domain.split("\\.");
        System.out.println(domain + " has " + domainParts.length + " parts");

        for (int i = 0; i<domainParts.length; i++) {
            System.out.println("Writing: " + domainParts[i]);
            byte[] domainBytes = domainParts[i].getBytes("UTF-8");
            dataOS.writeByte(domainBytes.length);
            dataOS.write(domainBytes);
        }

        // No more parts
        dataOS.writeByte(0x00);

        // Type 0x01C = AAAA (ipv6 Host Request)
        dataOS.writeShort(0x001C);

        // Class 0x01 = IN
        dataOS.writeShort(0x0001);

        byte[] dnsFrame = byteArrayOS.toByteArray();

        System.out.println("Sending: " + dnsFrame.length + " bytes");
        for (int i =0; i< dnsFrame.length; i++) {
            System.out.print("0x" + String.format("%x", dnsFrame[i]) + " " );
        }

        // *** Send DNS Request Frame ***
        socket = new DatagramSocket();
        DatagramPacket dnsReqPacket = new DatagramPacket(dnsFrame, dnsFrame.length, ipAddress, DNS_SERVER_PORT);
        socket.send(dnsReqPacket);
    }
    
    
    public static void MXSetup() throws IOException {
    	ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        DataOutputStream dataOS = new DataOutputStream(byteArrayOS);

        // *** Build a DNS Request Frame ****

        // Identifier
        dataOS.writeShort(0x1234);

        // Write Query Flags
        dataOS.writeShort(0x0100);

        // Question Count
        dataOS.writeShort(0x0001);

        // Answer Record Count
        dataOS.writeShort(0x0000);

        // Authority Record Count
        dataOS.writeShort(0x0000);

        // Additional Record Count
        dataOS.writeShort(0x0000);

        String[] domainParts = domain.split("\\.");
        System.out.println(domain + " has " + domainParts.length + " parts");

        for (int i = 0; i < domainParts.length; i++) {
            System.out.println("Writing: " + domainParts[i]);
            byte[] domainBytes = domainParts[i].getBytes("UTF-8");
            dataOS.writeByte(domainBytes.length);
            dataOS.write(domainBytes);
        }

        // No more parts
        dataOS.writeByte(0x00);

        // Type 0xF = MX (MX Host Request)
        dataOS.writeShort(0xF);

        // Class 0x01 = IN
        dataOS.writeShort(0x0001);

        byte[] dnsFrame = byteArrayOS.toByteArray();

        System.out.println("Sending: " + dnsFrame.length + " bytes");
        for (int i =0; i< dnsFrame.length; i++) {
            System.out.print("0x" + String.format("%x", dnsFrame[i]) + " " );
        }

        // *** Send DNS Request Frame ***
        socket = new DatagramSocket();
        DatagramPacket dnsReqPacket = new DatagramPacket(dnsFrame, dnsFrame.length, ipAddress, DNS_SERVER_PORT);
        socket.send(dnsReqPacket);
    }
    
    
    public static void reverseDNSFetch() throws IOException {
    	ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        DataOutputStream dataOS = new DataOutputStream(byteArrayOS);
       // *** Build a DNS Request Frame ****

        // Identifier
        dataOS.writeShort(0x1234);

        // Write Query Flags
        dataOS.writeShort(0x0100);

        // Question Count
        dataOS.writeShort(0x0001);

        // Answer Record Count
        dataOS.writeShort(0x0000);

        // Authority Record Count
        dataOS.writeShort(0x0000);

        // Additional Record Count
        dataOS.writeShort(0x0000);
        System.out.println("above ip split");
        String[] ipSplit = domain.split("\\.");
        System.out.println(ipSplit);
        String reverseMessage = "";
        for(int i = ipSplit.length - 1; i>=0; i--)  {
        	reverseMessage = reverseMessage + ipSplit[i] + ".";
        }
        reverseMessage =  reverseMessage + "in-addr.arpa";
        System.out.println(reverseMessage);
        String[] domainParts = reverseMessage.split("\\.");
        System.out.println(reverseMessage + " has " + domainParts.length + " parts");

        for (int i = 0; i < domainParts.length; i++) {
            System.out.println("Writing: " + domainParts[i]);
            byte[] domainBytes = domainParts[i].getBytes("UTF-8");
            dataOS.writeByte(domainBytes.length);
            dataOS.write(domainBytes);
        }
        
        // No more parts
        dataOS.writeByte(0x00);

        // Type 0xC = PTR (Reverse DNS Request)
        dataOS.writeShort(0xc);

        // Class 0x01 = IN
        dataOS.writeShort(0x0001);
        
        byte[] dnsFrame = byteArrayOS.toByteArray();

        System.out.println("Sending: " + dnsFrame.length + " bytes");
        for (int i =0; i< dnsFrame.length; i++) {
            System.out.print("0x" + String.format("%x", dnsFrame[i]) + " " );
        }

        // *** Send DNS Request Frame ***
        socket = new DatagramSocket();
        DatagramPacket dnsReqPacket = new DatagramPacket(dnsFrame, dnsFrame.length, ipAddress, DNS_SERVER_PORT);
        socket.send(dnsReqPacket);
    }
    
    

    public List<String> getIP4List() {
    	return address4List;
    }
    
    public List<String> getIP6List() {
    	return address6List;
    }
    
    public List<String> getMXList() {
    	return MXList;
    }
    
    public List<String> getCNAMEList() {
    	return cnameList;
    }
    
    public String getDomain() {
    	return domain;
    }
    
    
    
    /**
     * Create an Alert Box for warning messages
     * @param
     * 		Takes in a String which represents an warning message
     */
    public void warningAlert(String warningMessage){
    	Alert alert = new Alert(AlertType.WARNING);
    	//Setting the AlertBox title
    	alert.setTitle("Incorrect Input Error");
    	alert.setHeaderText(null);
    	alert.setContentText(warningMessage);
    	alert.showAndWait();
    }
    
    public static boolean ip(String text) {
        Pattern p = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
        Matcher m = p.matcher(text);
        return m.find();
    }
}
