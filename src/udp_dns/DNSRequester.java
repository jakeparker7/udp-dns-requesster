package udp_dns;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * This class provides the main method that runs the event allocation program.
 * 
 * INSTRUCTIONS: DO NOT MODIFY THIS CLASS
 */
public class DNSRequester extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        DNSRequest DNSRequest = new DNSRequest("");
        UDPView view = new UDPView(DNSRequest);
        new UDPController(DNSRequest, view);

        stage.setScene(view.getScene());
        stage.setTitle("Event Allocator");
        stage.show();
    }

}